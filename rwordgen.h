#ifndef _RWORDGEN_H
#define _RWORDGEN_H
#include <stdio.h>

void linear ( char * word, unsigned long amount, int wordlength, 
        unsigned int maxlen, FILE * output );
void full_random ( char * word, unsigned long amount, unsigned int maxlen,
        FILE * output);
void dictionary ( unsigned long amount, FILE * dictfile,
       FILE * output, unsigned int maxlen );


#endif // _RWORDGEN_H
