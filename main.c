#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "rwordgen.h"

void print_help ( char * programname );

int main (int argc, char ** argv) {
    FILE * output;
    FILE * dictfile;
    char * word;
    unsigned long amount = 1000;
    unsigned int wordlength = 0;
    int random = 0;
    int dict = 0;
    unsigned int maxlen = 184;

    output = stdout;
    dictfile = NULL;

    //go through arguments and parse them
    for (int j=0; j < argc; j++) {
        //sets flag to fully random "words" instead of linear incrementation
        if (!(strcmp(argv[j], "--full-random")) || !(strcmp(argv[j], "-fr"))) {
            random = 1;
        }

        if (!(strcmp(argv[j], "--dictionary")) || !(strcmp(argv[j], "-d"))) {
            dict = 1;
            if ((dictfile = fopen(argv[j+1], "r")) == NULL) {
                puts("couldn't open dictionary file");
                return 0;
            }
        }

        //reallocate the size of character pointer if needed
        if (!(strcmp(argv[j], "--max-length")) || !(strcmp(argv[j], "-m"))) {
            maxlen = strtol(argv[j+1], NULL, 10);
            word = (char*)realloc(word, maxlen * sizeof(char));
        }

        //set the starting word, only used in linear mode
        if (!(strcmp(argv[j], "--start")) || !(strcmp(argv[j], "-s"))) {
            if (j+1 <= argc) {
                strcpy(word, argv[j+1]);
                wordlength = strlen(word) - 1;
                for (unsigned int k=0; k<strlen(word); k++) {
                    if (word[k] < 'a' || word[k] > 'z'){
                        puts("given starting word invalid");
                        print_help(argv[0]);
                        return 0;
                    }
                }
            }
        }

        //set output file
        if (!(strcmp(argv[j], "--output")) || !(strcmp(argv[j], "-o"))) {
            if ((output = fopen(argv[j+1], "w")) == NULL) {
                puts("couldn't open file");
                return 0;
            }
        }
        //set amount of loops
        if (!(strcmp(argv[j], "--amount")) || !(strcmp(argv[j], "-a"))) {
            amount = strtol(argv[j+1], NULL, 10);
        }
        //print help
        if (!(strcmp(argv[j], "--help")) || !(strcmp(argv[j], "-h"))) {
            print_help(argv[0]);
            return 0;
        }
   }

    word = (char*)malloc(maxlen * sizeof(char));
    strcpy(word, "a");

    if (wordlength+1 > maxlen) {
        puts("Given starting word is longer than given maximum length");
        free(word);
        return 0;
    }
    if (dict) {
        dictionary(amount, dictfile, output, maxlen);
    }

    if (random) {
        full_random(word, amount, maxlen, output);
    }

    if (!random && !dict) {
        linear(word, amount, wordlength, maxlen, output);
    }
    if (output != stdout) {
        fclose(output);
    }

    free(word);
    return 0;
}

//print help with --help
void print_help ( char * programname) {
    printf("\nUsage: %s [OPTIONS]\n", programname);
    printf("Prints out \"words\" iterating up from last letter.\n");
    printf("Without options starts from \'a\' and iterates it up 1000 times.\n\n");
    printf("Optional arguments:\n");
    printf("%5s, %12s      print this help\n", "-h", "--help");
    printf("%5s, %12s      set the amount of words printed. Example: -a 10\n", "-a", "--amount");
    printf("%5s, %12s      set the starting word. Example: -s poop\n", "-s", "--start");
    printf("%5s, %12s      set the maximum length for word. Example: -m 8\n", "-m", "--max-length");
    printf("%5s,%12s      generate random words with variable length and letters\n", "-fr", "--full-random");
    printf("%5s,%12s      set filename to print the words to. Example: -o wordspam.txt\n\n", "-o", "--output");
}


