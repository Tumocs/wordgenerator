# RWORDGEN

Rwordgen is a simple program that generates random words.
It either iterates from given starting words (a by default) or produces
completely random words with variable length.
rwordgen can be compiles as a standalone program or library.

## Building

To build rwordgen as standalone program run

make

To build rwordgen as dynamic library run

make library

## Usage

run program with --help to get more help about command arguments
