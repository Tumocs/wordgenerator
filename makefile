CC = gcc
CFLAGS = -Wall -Wextra -Wpedantic

rwordgen : main.o rwordgen.o

main.o : main.c

rwordgen.o : rwordgen.c

clean:
	rm -fv rwordgen rwordgen.o main.o rwordgen.so

library: librwordgen.o
	$(CC) -shared -o librwordgen.so librwordgen.o

librwordgen.o : rwordgen.c
	$(CC) -c -fpic -o librwordgen.o rwordgen.c
