#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "rwordgen.h"

/* linear word function. Increments the word from a upwards adding
 * one more letter when first letter is at z
 * takes word pointer, loop amount, current word length, maximum word length
 * and output file (stdout by default) as arguments */
void linear ( char * word, unsigned long amount, int wordlength, 
       unsigned int maxlen, FILE * output ) {
    unsigned long counter = 0;
    int i = wordlength;
    //the magic loop
    while (counter < amount && strlen(word) <= maxlen) {
        fprintf(output, "%s\n", word);
        if(word[i] <= 'z')
            word[i]++;
        for(int j=i; j>0; j--) {
            if(word[j] > 'z') {
                word[j-1]++;
                word[j] = 'a';
            }
        }
        if(word[0] > 'z') { 
            i++;
            word[i] = 'a';
            word[i+1] = '\0';
            word[0] = 'a';
        }

        counter++;
    }
}

/* random word function. prints words with varied length with
 * given maximum, default being 184 letters. takes character pointer,
 * amount of loops, maximum length and output file (stdout by default)
 * as arguments */
void full_random ( char * word, unsigned long amount, unsigned int maxlen,
       FILE * output ) {
    unsigned long counter = 0;
    int wordlength;
    srand(time(NULL));
    while ( counter < amount ) {
        wordlength = (rand() % maxlen) + 1;
        for (int j = 0; j < wordlength; j++) {
            word[j] = 97 + (rand() % 25);
        }
        word[wordlength] = '\0';
        fprintf(output, "%s\n", word);
        counter++;
    }
}

/* gets random word from dictionary file where words are separated
 * by newline, in example /usr/share/dict/words in some linux
 * distributions. Function takes amount of words to get, dictionary
 * file in readmode, output file (can be stdout) and maximum length
 * of possible words. */
void dictionary ( unsigned long amount, FILE * dictfile,
        FILE * output, unsigned int maxlen ) {
    int lines = 0;
    unsigned long counter = 0;
    char ** words;
    int ch;
    int random;
    int length;
    srand(time(NULL));

    //calculate words assuming they are separated by newline
    while ((ch = fgetc(dictfile)) != EOF) {
        if (ch == '\n') {
            lines++;
        }
    }
    /* create structure from words to make access to words faster
     * when handling large amount of word requests*/
    words = (char**)malloc(lines * (sizeof(char*)));
    for (int i=0; i<lines; i++) {
        words[i] = (char*)malloc(maxlen * sizeof(char));
    }
    //printf("%d\n", lines);
    rewind(dictfile);
    for (int i=0; i<lines; i++) {
        fgets(words[i], maxlen, dictfile);
    }
    fclose(dictfile);

    //get word and clean whitespace from words before output
    while (counter < amount) {
        random = rand() % lines;
        length = strlen(words[random]);
        for (int i = 0; i <= length; i++) {
            if (!(words[random][i] >= 'A' && words[random][i] <= 'Z') &&
                !(words[random][i] >= 'a' && words[random][i] <= 'z') &&
                !(words[random][i] == '\''))
                words[random][i] = '\0';

        }
        if (strlen(words[random]) > 0)
            fprintf(output, "%s\n", words[random]);
        counter++;
    }

    for (int i=0; i<lines; i++) {
        free(words[i]);
    }
    free(words);
}
